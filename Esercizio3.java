/**
 * Nome e Cognome: Matteo Tamari 
 * Matr: 0000843272
 * email: matteo.tamari@studio.unibo.it
 */

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Locale;

/**
 * Classe dell'Esercizio 3 Costo computazionale dato dai cicli per il
 riempimento della matrice e non essendoci riodinamento dei vettori O(numberOfAssignment*optimalMatrix),
 sarebbe 16*O(numberOfAssignment*optimalMatrix) ma essendo una costante moltiplicativa che secondo
 le istruzioni e' fissa non la tengo in considerazione, quindi O(n*m).
 */
public class Esercizio3 {

    //@numberOfAssignment numero di assignment 
    public static int numberOfAssignment;
    //@crediti crediti  
    public static int[] crediti;
    //@deadline deadline 
    public static int[] deadline;
    //@optimalMatrix[][] matrice per il riempimento con deadline e crediti 
    public static int [][] optimalMatrix;

    /**
     * Metodo per la lettura del file che riceve i dati e salva nelle variabili
     * globali per poterle utilizzare anche in altri meotodi
     *
     * @param inputFile prende in input il file richiesto
     */
    public static void lettura(String inputFile) {
        Scanner file = null;
        try {
            file = new Scanner(new FileReader(inputFile));
        } catch (FileNotFoundException e) {
            System.out.println("errore nella lettura del file");
        }
        //tengo fuori l'assigment in modo che assegni una volta sola il valore alla variabile che mi servira per produrre la matrice
        //ovvero al sua dimensione, si tratta del primo valore nel file di input  
        numberOfAssignment = file.nextInt();
        System.out.println(" abbiamo " + numberOfAssignment + " deadline e crediti da valutare");
        //fornisco la dimensione all'array dei crediti 
        crediti = new int[numberOfAssignment];
        //stessa cosa per l'array delle deadline 
        deadline = new int[numberOfAssignment];
        //ciclo che mi scorre il file per poi assegnare nelle varie delle dei crediti e deadline i valori 
        for (int i = 0; i < numberOfAssignment; i++) {
            if (i < numberOfAssignment) {
                crediti[i] = file.nextInt();
                deadline[i] = file.nextInt();
            }
        }

    }

    /**
     * Metodo per valutazione delle migliori casistiche della coppia deadline e
 crediti, questo metodo si basa su una matrice 2 righe e 16 colonne (in
 quanto da istruzioni risultava essere la grandezza massima richiesta per
 le migliori casistiche), la valuazione e strutturata sulla lunghezza di
 "numberOfAssignment" in modo tale prendere in considerazione tutte le deadline e crediti
 disponibili, tutte le volte che incontro una deadline con crediti
 migliori a questo punto la inserisco nella mia matrice, se la spazio e vuoto puo' esserci un
 caso che per una certa deadline non vi sia un credito migliore oppure che
 proprio non vi sia, per questo motivo col secondo ciclo scorro al
 contrario valutando se ci sono migliori opportunita' o buchi vuoti e
 quindi valutare una scelta ottima in corrispondenza.Essendo un matrice di
 default gestira' il caso nella quale ci siano deadline e creiditi di 0
 totali, come richiesto nella mail supplementare.
     *
     * @param crediti vettore per i crediti, vengono passati da una variabile
     * globale
     * @param deadline vettore per le deadline, vengo passati da una variabile
     * globale
     * @param output e' il mio file di stampa che stampa sia il totale dei
     * crediti che la scelta ottima
     */
    public static void matrixAdd(int crediti[], int deadline[], String output) {
        optimalMatrix = new int[2][16];
        //credo le seguenti variabili di appoggio per trasferire i migliori crediti e deadline
        int deadlineApp2;
        int creditiApp2;
        int deadlineApp;
        int creditiApp;
        //varibile che mi servira per stampare il totale dei crediti 
        int creditiTotali = 0;
        //ciclo per scorrere tutte le possibili scelte salvate nella variabile globale "numberOfAssignment"
        for (int i = 0; i < numberOfAssignment; i++) {
            //salvo in queste due varibili i crediti e le deadline che incontro mi serviranno nel ciclo successivo per 
            //valutare se sono migliori di quelle inserite nella matrice 
            deadlineApp = deadline[i];
            creditiApp = crediti[i];
            //in questo ciclo vi e in nucleo del programma, qua effettuo i cambiamenti nelle varie celle grazie alle variabili 
            //di appoggio che mi consentono poi si assegnare le migliori deadline e crediti all'interno della mia matrice "optimalMatrix"
            for (int j = deadline[i] - 1; j >= 0; j--) {
                if (creditiApp > optimalMatrix[0][j]) {
                    //se i creiditi del ciclo precendente sono migliori allora salvo nella cella rispettiva le 
                    //App2 
                    creditiApp2 = optimalMatrix[0][j];
                    deadlineApp2 = optimalMatrix[1][j];
                    //dopo di che che dico che i valori rispettivi alle celle della matrice sono uguali ad App
                    optimalMatrix[0][j] = creditiApp;
                    optimalMatrix[1][j] = deadlineApp;
                    //a questo punto assegno i valori a creditiApp e deadlineApp
                    creditiApp = creditiApp2;
                    deadlineApp = deadlineApp2;               
                    //questo if mi serve nel caso le deadline e i crediti siano entrambi uguali a 0 
                    if (optimalMatrix[0][j] == 0 && optimalMatrix[1][j] == 0) {
                        j--;
                    }
                }
            }
        }
        //a questo punto ho ottenuto le mie deadline e crediti migliori e procedo alla stampa su file 
        PrintWriter newFile = null;
        try {
            newFile = new PrintWriter(output);
        } catch (FileNotFoundException e) {
            System.out.println("errore nella scrittura del file");
        }
        //questo ciclo scorre tutti i crediti e li somma alla variabile di appoggio creata sopra 
        for (int j = 0; j < 16; j++) {
            creditiTotali += optimalMatrix[0][j];
        }
        //stampo la somma dei crediti totali 
        newFile.println(creditiTotali);
        //stampo le deadline e crediti migliori comprese tra 1 e 16
        for (int i = 0; i < 16; i++) {
            newFile.write(optimalMatrix[0][i] + " " + optimalMatrix[1][i] + "\n");

        }
        System.out.println("ecco la matrice del caso ottimo");
        System.out.println(Arrays.deepToString(optimalMatrix));
        newFile.close();
    }

    /**
     * Metodo main per l'esecuzione del programma
     *
     * @param args
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        lettura(args[0]);
        matrixAdd(crediti, deadline, args[1]);

    }

}
