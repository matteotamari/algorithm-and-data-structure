/**
 * Nome e Cognome: Matteo Tamari
 * Matr: 0000843272
 * email: matteo.tamari@studio.unibo.it
 */

import java.util.Scanner;
import java.util.Stack;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.Locale;

/**
 * Classe del progetto Esercizio 1 Costo computazione dato dal ciclo for che si
 * occupa di scorrerre tutte le volte la stringa allTheLine, quindi O(n*m/2),
 * dove "m" e la lunghezza del mio typeOfbrackets contenete i caratteri di
 * apertura e chiusura .
 */
public class Esercizio1 {

    //@ allTheLine variabile globale sulla quale salvo la stringa che sara' oggetto della analisi dei caratteri "parentesi"
    public static String allTheLine;
    //@'manyTypeOfbrackets' salvo al suo interno il numero di possibili parentesi che il file fornisce
    public static int manyTypeOfbrackets;
    // @typeOfbrackets salvo al suo interno le tipologie di parentesi che saranno oggetto della analisi, lo inzializzo a null
    //in quanto sara' il meotodo di riferimento manyTypeOfbrackets definire la dimensione
    public static char[] typeOfbrackets = null;
    // @ stack lo stack o pila viene sfruttato il quanto possiede le proprieta' LIFO (ovvero aggiungo e tolgo elementi soltanto da un estremo)
    //lo stack in oggetto viene utilizzato per carpire se le parentesi aperte vengono poi effettivamente chiuse,con le operazioni
    // push() e pop(). Utilizzo anche peek() per valutare che cosa ho inserito in un preciso momento del programma, mentre empty() \
    //viene utilizzato per valutare se e' vuoto o meno e stampare manyTypeOfbrackets seconda del caso il risultato finale della analisi.
    public static Stack stack = null;
    // @index all'interno di questa variabile vi salvo il risultato del meotodo getIndex(), viene utilizzata per compiere una valutazione
    //in quale parte del nostro typeOfbrackets/2 ci troviamo, sx o dx.
    public static int index;
    //@ variabile contatore per contare quanti inserimenti ho fatto nello stack
    public static int countAdd;
    //@ variabile contatore per contare quante rimozioni ho fatto nello stack
    public static int countRm;

    /**
     * Metodo che prende in input un file da tastiera, il quale viene letto per
     * consentiere di ottenere i dati utili alla costruzione del programma
     * utilizzo variabili globali sulla quale salvo i dati per comodita'.
     *
     * @param inputFile prende in input un file e lo da in pasto al metodo
     */
    public static void lettura(String inputFile) {
        Scanner file = null;
        try {
            file = new Scanner(new FileReader(inputFile));
        } catch (FileNotFoundException e) {
            System.out.println("errore lettura file");
        }
        while (file.hasNextLine()) {
            manyTypeOfbrackets = Integer.parseInt(file.nextLine());
            System.out.println("Tipi parentesi: " + manyTypeOfbrackets);
            typeOfbrackets = file.nextLine().replace(" ", "").toCharArray();
            System.out.println(Arrays.toString(typeOfbrackets));
            allTheLine = file.nextLine();
            System.out.println(allTheLine);
        }
        file.close();
    }

    /**
     * Motodo per la scrittura su file del risultato ottenuto dal programma
     *
     * @param output prende in output il nome del file che comparira' in
     * cartella
     */
    public static void scrittura(String output) {
        PrintWriter newFile = null;
        try {
            newFile = new PrintWriter(output);
        } catch (FileNotFoundException e) {
            System.out.println("Il file non e' stato scritto");
        }
        //non conto un numero di parentesi aperte successive pari al numero delle parentesi chiuse che ho incontrato
        System.out.println("ecco lo stack ");
        System.out.println(stack.empty());
        if (stack.empty()) {
            if (countAdd == 0) {
                newFile.print("-1");
            }
            newFile.println(countAdd);
        } else {
            newFile.println("-1");
        }
        newFile.close();
    }

    /**
     * Metodo che mi ritorna un indice con la quale compio le verifiche sulla
     * stringa allTheLine, questo mi serve per capire se il carattere che sto
     * valutando e' e contenuto nel mio typeOfbrackets di caratteri. La
     * variabile e' inizializzata al minimo intero poiche' se il carattere non
     * viene trovato allora non appartiene ai caratteri di apertura o chiusura
     *
     * @param array typeOfbrackets dentro la quale gli viene passato il vettore
     * typeOfbrackets[] come variabile globale
     * @param carattere variabile all'interno della quale gli passo il carattere
     * che in quel momento sto valutando
     * @return indice
     */
    public static int getIndex(char[] array, char carattere) {
        int indice = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (carattere == array[i]) {
                indice = i;
            }
        }
        System.out.println("ecco il mio indice");
        System.out.println(indice);
        return indice;
    }

    /**
     * Metodo Main nella quale compio tutte le operazioni di verifica, creando
     * l'oggetto stack e la variabile index creo un primo ciclo dove scorro
     * tutta allTheLine la quale contiene al suo intero la terza
     * "riga" che il file di input fornisce . Come parametro della variabile
     * index gli passo il valore indice ottenuto dal metodo getIndex(), se index
     * e' minore di typeOfbrackets/2 (questo perche' dividendo
     * typeOfbrackets a meta' so che nella parte di sx ho caratteri
     * di apertura e nella parte di dx ho quelle di chiusura) e inoltre diverso
     * da MIN_VALUE che non un numero di posizione dell'typeOfbrackets(questo
     * vuol dire che il mio carattere non esiste), allora e' un carattere di
     * apertura e quindi inserisco nello stack e se la variabile contatore
     * countRM e = 0 allora posso incrementare la variabile countAdd altrimenti
     * decremento countRm poiche' potrei insesire una parentesi che non e'
     * corretta. Se invece ho un valore index > di typeOfbrackets/2, significa
     * che sono nella parte dx del mio typeOfbrackets quindi in un carattere di
     * chiusura, a questo punto controllo che il valore che
     * sulla cima dello stack sia corrispondente a quello che
     * nell'typeOfbrackets in posizione index - manyTypeOfbrackets (cosi torna
     * indietro delle posizioni utili e valuta se e' il suo omonimo contrario ad
     * esempio: valuto se "(" ---> corrisponde manyTypeOfbrackets ")", se cosi
     * e' elimino dallo stack il carattere e procedo cosi fino alla fine della
     * sequenza di allTheLine.
     *
     * @param args
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        stack = new Stack<Integer>();
        lettura(args[0]);
        for (int i = 0; i < allTheLine.length(); i++) {
            //assegno il valore indice del metodo alla variabile index
            index = getIndex(typeOfbrackets, allTheLine.charAt(i));
            // se e' minore e' un crattare di apertura allora lo inserisco nello stack, index deve essere diverso da MIN_VALUE
            //altrimenti l'operazione non puo' avvenire poiche e' un carattere non di posizione e quindi non esiste.
            if (index < (typeOfbrackets.length / 2) && index != Integer.MIN_VALUE) {
                //se l'if e' true allora inserisco nello stack il carattere che sto valutando
                stack.push(allTheLine.charAt(i));
                System.out.println(stack.peek());
                //se countRm e uguale manyTypeOfbrackets 0 allora incremento il countAdd
                if (countRm == 0) {
                    countAdd++;
                    System.out.println("stampo countAdd quando countRm e' vuoto");
                    System.out.println(countAdd);
                } else {
                    //altrimenti lo decremento
                    countRm--;

                    System.out.println("stampo countRm nell'if del file di stampa quando decremento");
                    System.out.println(countRm);
                }

                System.out.println("Sto inserendo questo carattere " + stack.peek());
                //se e' maggiore e' un carattere di chiusura, allora devo verificare se si trova sulla cima dello stack e procedere con la rimozione
            } else if (index >= (typeOfbrackets.length / 2)) {
                System.out.println("Il carattere di chiusura e': " + typeOfbrackets[index] + "Il corrispondente di apertura sarebbe: " + typeOfbrackets[index - manyTypeOfbrackets]);
                try {
                    //se il carattere oggetto della valutazione corrisponde manyTypeOfbrackets quello che sta in posizione nell'typeOfbrackets all'indice dato da index -manyTypeOfbrackets
                    //allora rimuovo il carattere poiche' si tratta del suo omonimo contrario.
                    if ((char) stack.peek() == typeOfbrackets[index - manyTypeOfbrackets]) {
                        System.out.println("sto tirando fuori questo carattere " + stack.peek());
                        stack.pop();
                        countRm++;
                        System.out.println("stampo countRm nell'if dei caratteri di chiusura quando rimuovo dallo stack");
                        System.out.println(countRm);

                    }

                } catch (EmptyStackException e) {

                }
            }
        }
        System.out.println(stack.empty());

        scrittura(args[1]);

    }
}
