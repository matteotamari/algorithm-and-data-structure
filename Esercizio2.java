/**
 * Nome e Cognome: Matteo Tamari
 * Matr: 0000843272
 * email: matteo.tamari@studio.unibo.it
 */

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Locale;
import java.util.InputMismatchException;

/**
 * Classe del proggetto Esercizio 2
 * Il costo computazione dettato dalla "creazione" del nostro albero attraverso
 * i nodi del nostro albero O(n), anche per l'algoritmo ricorsivo di valutazione
 * se si tratta di un BST o meno e' O(n)
 */
public class Esercizio2 {

    //@caratteri Stringa per la valutazione del tipo di intero (figlio dx, sx)
    public static String caratteri;
    //@root utilizzato nel metodo di lettura come variabile globale per esemplificare il processo e l'assegnamento dei valori
    public static Node root;

    /**
     * classe per la modellazione del concetto di Nodo. Un nodo ha:
     * -radice
     * -figlio sinistro
     * -figlio destro
     * la variabile "dad" voene utilizzato come
     * "appoggio" per i sotto alberi creati questo perche' un nodo figlio puo'
     * essere a sua volta padre e le condizioni BST devono essere rispettate
     */
    public static class Node {

        //radice utilizzo integer per gestire e il null la chiamo radice per
        //per evidenziare la differenza con la globale posta all'esterno di
        //questa classe
        private Integer radice;
        //padre come appoggio per ogni sottoalbero radicato
        private Node dad;
        // figlio sinistro
        private Node sx;
        // figlio destro
        private Node dx;

        public Node(Integer radice) {
            this.radice = radice;
            sx = null;
            dx = null;
            dad = null;
        }

        public Integer getValue() {
            return radice;
        }

        public void setLeftChild(Node child) {
            sx = child;
        }

        public void setRightChild(Node child) {
            dx = child;
        }

        public Node getLeftChild() {
            return sx;
        }

        public Node getRightChild() {
            return dx;
        }

        public void setParent(Node parent) {
            this.dad = parent;
        }

        public Node getParent() {
            return dad;
        }
    }

    /**
     * Metodo boolean che valuta se l'albero e' binario oppure no, prendendo in
     * input dei parametri "node", "minimo" , "massimo".
     * La valutazione che ho fatto e' stata suppore
     * che a destra della root tutto e compreso tra la root stessa e + infinito,
     * mentre a sinistra della root tutto e compreso tra la root e - infinito,
     * per i sottoalberi radicati il ragionamento e' un po' diverso per i figli
     * alla estrema destra o estrema sinistra dela root il caso cambia solo per
     * il fatto che il valore dovra' essere compreso tra il padre e - o +
     * infinito a seconda di dove ci troviamo (nel grafico se 70 e' compreso tra
     * [-inf,100]), per quanto riguarda i nodi radicati internamente verfico se
     * il nodo e' compreso tra il padre e il nonno (nel grafico sotto se 80 e
     * compreso tra [70,100]), se lo e' allora le condizioni sono rispettate.
     *                          -inf 100 +inf
     *                               /  \
     *                        -inf 70    140 + inf
     *                            /  \   /  \
     *                      -inf 60 80 130 170 + inf
     *
     * @param node oggetto della classe node che mi serve per valutare e
     * confrontare durante la costruzione: la root, il figlio sinistro e figlio
     * destro
     * @param minimo e' il mio limite inferiore
     * @param massimo e' il mio limite superiore
     * @return isAbinarySearchTree
     */
    public static boolean isAbinarySearchTree(Node node, int minimo, int massimo) {
        //se l'albero oppure il valore del nodo sono nulli, allora ritorna true
        if (node == null || node.getValue() == null) {
            System.out.println("sono entrato nel node == null");
            return true;
        }
        //verifico se le condizioni BST sono rispettate, se il figlio sinistro e maggiore della root o dad e il figlio destro e' minore non e' BST quindi ritorno false
        System.out.println("Key= " + node.radice + "min= " + minimo + "max= " + massimo);
        if (node.radice < minimo || node.radice > massimo) {
            return false;
        }
        //se le condizioni BST sono rispettate, allora procedo ricorsivamente a verificare se anche i figli dei sottoalberi radicati hanno le proprieta' BST
        return (isAbinarySearchTree(node.dx, node.radice, massimo) && isAbinarySearchTree(node.sx, minimo, node.radice));

    }

    /**
     * Metodo per la lettura del file che mi serve per salvare i dati contenuti
     * nel file da valutare nella analisi e costruzione del programma, al suo
     * interno oltre la lettura vi e la valutazione del tipo di dato, il primo
     * elemento so per certo che sara' la root per tanto scarto la prima
     * parentesi, dopo di che salvo il primo intero nella variabile root che
     * tengo come monito per i successivi confronti, sapro' cosi sempre se
     * le condizioni sono rispettate anche con la root, attraverso gli if
     * valutiamo se si tratta di un figlio sinistro e
     * destro, che a loro volta possono diventare sottoaberi radicati e quindi
     * root. Con i caratteri "(" oppure "," opuure ")" valutiamo se si tratta di
     * un figlio sinistro o destro.
     *
     * @param inputFile file preso in input.
     */
    public static void lettura(String inputFile) {
        Scanner file = null;
        try {
            file = new Scanner(new FileReader(inputFile));
        } catch (FileNotFoundException e) {
            System.out.println("errore nella lettura del file");
        }
        file.next();
        //converto la root in un Integer per gestire il null;
        root = new Node(Integer.parseInt(file.next()));
        while (file.hasNext()) {
            //salvo il primo carattere per verificare di che tipo di dato si tratta
            caratteri = file.next();
            //Se e' una "(" allora si tratta di un figlio sinistro o fratello
            if (caratteri.equals("(")) {
                //creo l'oggetto sx inzializzandolo a null per gestire il caso nella quale sia presente
                //il carattere "-"
                Node sx = null;
                try {
                    sx = new Node(file.nextInt());
                } catch (InputMismatchException e) {
                    sx = new Node(null);
                }
                //System.out.println("Sto inserendo a sinistra " + sx.getValue() + "suo padre e' " + root.getValue());
                //setto a questo il nodo sinistro al valore della root in quanto parent
                sx.setParent(root);
                //quindi setto come figlio sinistro dell root in questione sx
                root.setLeftChild(sx);
                //a questo punto salvo sx come suo figlio sinistro
                root = root.getLeftChild();
                //Se il carattere e' "," allora e un figlio destro o fratello destro
            } else if (caratteri.equals(",")) {
                //creo l'oggetto dx inizializzandolo a null per gestire il caso nella quale sia presente
                //il carattere "-"
                Node dx = null;
                try {
                    dx = new Node(file.nextInt());
                } catch (InputMismatchException e) {
                    dx = new Node(null);
                }
                //setto il valore prendendo il parent di root
                root = root.getParent();
                //System.out.println("sto inserendo a destra " + dx.getValue() + "suo padre e' " + root.getValue());
                //stessa assegnazione speculare a sopra ma con dx
                dx.setParent(root);
                //stessa assegnazione speculare a sopra ma con dx
                root.setRightChild(dx);
                // a questo punto dico che il valore di root e' uguale a dx
                root = dx;
                //questo if e' necessario per gli ultimi paramentri altrimenti quelli in fondo potrebbero non essere
                //inclusi nel BST
            } else if (caratteri.equals(")")) {
                if (root.getParent() != null) {
                    root = root.getParent();
                }
            }

        }
    }

    /**
     * Metodo per scrivere su file utilizzando il metodo isAbinarySearchTree()
     * che stampa true o false
     *
     * @param output nome del file output
     */
    public static void scrittura(String output) {
        PrintWriter newFile = null;
        try {
            newFile = new PrintWriter(output);
        } catch (FileNotFoundException e) {
            System.out.println("errore nella scrittura del file");
        }
        //uso MIN_VALUE per il limite inferiore e MAX_VALUE per il limite superiore
        newFile.print((isAbinarySearchTree(root, Integer.MIN_VALUE, Integer.MAX_VALUE)));
        newFile.close();
    }

    /**
     * Metodo main per l'esecuzione del programma
     *
     * @param args
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        lettura(args[0]);
        scrittura(args[1]);
        System.out.println(root.getValue());
        System.out.println(isAbinarySearchTree(root, Integer.MIN_VALUE, Integer.MAX_VALUE));
    }

}
