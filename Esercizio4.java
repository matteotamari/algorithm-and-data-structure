/**
 * Nome e Cognome: Matteo Tamari
 * Matr: 0000843272
 * email: matteo.tamari@studio.unibo.it
 */
import java.util.Scanner;
import java.util.Locale;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Classe del progetto Esercizio 4 Costo computazionale dato dal ciclo
 * all'interno del while e dal while stesso O(n*m) ovvero il numero di resto che
 * devo erogare per il numero di monete che ho a disposizione
 */
public class Esercizio4 {

    //@q metri cubi di gas 
    public static int gas;
    //@c capacita' della cisterna 
    public static int capacity[];
    //tipi di cisterne 
    public static int typeOfContainer;
    //ArrayList mi serve per verificare al suo interno il valore della moneta vista in quel momento
    public static ArrayList getMoney = new ArrayList();

    /**
     * Metodo per la lettura del file che mi serve per salvare i dati contenuti
     * nel file da valutare nella analisi e costruzione del programma.
     *
     * @param inputFile file preso da input
     */
    public static void lettura(String inputFile) {
        Scanner file = null;
        try {
            file = new Scanner(new FileReader(inputFile));
        } catch (FileNotFoundException e) {
            System.out.println("errore nella lettura del file");
        }
        gas = file.nextInt();
        typeOfContainer = file.nextInt();
        capacity = new int[typeOfContainer];
        for (int i = 0; i < typeOfContainer; i++) {
            if (i < typeOfContainer) {
                capacity[i] = file.nextInt();
            }
        }
        System.out.println("metri cubi di cisterne");
        System.out.println(gas);
        System.out.println("tipi di cisterne");
        System.out.println(typeOfContainer);
        System.out.println("queste sono le capacita'");
        System.out.println(Arrays.toString(capacity));

    }

    /**
     * Metodo per valutare a seconda delle quantita' date e monete disponibili
     * quale sia la scelta ottima in nello specifico caso
     *
     * @param capacity un array globale dove all'interno salvo
     * @param quantity quantita' di metri cubi di gas
     * @param output file di output che viene stampato
     */
    public static void printMyMoney(int capacity[], int quantity, String output) {
        PrintWriter newFile = null;
        try {
            newFile = new PrintWriter(output);
        } catch (FileNotFoundException e) {
            System.out.println("errore nella scrittura del file");
        }
        //iniziallizzo la stringa a -1 perche' con 0 potrei avere qualche problema nella scelta delle monete 
        String total = "-1\n";
        //la variabile di appoggio per contare quante volte entro nel ciclo e roperlo il caso non ci siano piu monete utili al resto
        int count = 0;
        //se la quantita' e' 0 oppure non vi e' capacita allora stampo -1
        if (quantity == 0 || capacity.length == 0) {
            System.out.println("-1");
            newFile.print("-1");
            newFile.close();
        }
        //se la quantita' e maggiore di 0 posso procedere con il ciclo e cominciare a verificare
        while (quantity > 0) {
            //se il mio contatore e' maggiore di 0 vuol dire che non sono entrato nel ciclo while sotto e quindi non ho monete per 
            //erogare il resto
            if (count > 0) {
                break;
            }
            //incremento count prima del suo ingresso nell'if eventuale
            count++;
            //fino a che la quantita' e' maggiore della capacita' allora opero sul ciclo
            System.out.println("Devo erogare " + quantity);
            for (int i = capacity.length - 1; i >= 0; i--) {
                //se la capacita' che sto guardando in quel momento e minore o unguale alla quantita che sto guardando in quel momento allora entro nell'if
                if (capacity[i] <= quantity) {
                    //inseriso nell ArrayList il valore della capacita' il quel momento 
                    getMoney.add(capacity[i]);
                    //aggiorno la quantita' sottraendole la capacita' della cisterna utilizzata 
                    quantity -= capacity[i];
                    System.out.println(capacity[i]);
                    Integer concatApp = capacity[i];
                    //uso la Stringa totale (che sono Final) per appendere o concatenare le monete ottenute, il tutto con un "\n" per mandare a capo
                    //ogni volta che utilizzo una moneta
                    //questo perche' cosi sono in grando di inserire successivamente la somma delle monete utilizzate nella prima riga del file di output 
                    total = total.concat(concatApp.toString() + "\n");
                    System.out.println(total);
                    //azzero la variabile count perche' potrei ancora avere delle monete con la quale 
                    //sottraggo la quantita'
                    count = 0;
                    //rompo il ciclo tutte le volte cosi che rincomincio da capo con i valori aggiornati e utilizzo le monete 
                    //anche se gia utilizzate qual'ora ci fosse una quantita in corrispondeza 
                    break;
                }
            }
        }
        //creo una variabile alla quale associo i valori di getMoney, cosi facendo posso utilizzare il metodo replace e sostituire alla 
        //inizializzazione iniziale della stringa, utilizzo Integer proprio per relazionarmi con il metodo replace di String  
        Integer concatApp = getMoney.size();
        //procedo alla sostituzione con replace 
        total = total.replace("-1", concatApp.toString());
        //stampo su file il valore finale di total che contiene il totale delle monetine e tutte quelle utilizzate 
        newFile.println(total);
        newFile.close();

        //ciclo test per verificare le monete che sto utilizzando 
        for (int i = 0; i < getMoney.size(); i++) {
            System.out.println(getMoney.get(i));
        }
    }

    public static void main(String args[]) {
        Locale.setDefault(Locale.US);
        lettura(args[0]);
        printMyMoney(capacity, gas, args[1]);

    }
}
